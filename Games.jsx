import React from 'react';
import { BrowserRouter, HashRouter } from 'react-router-dom';
import RSP from './RSP';
import Lotto from './Lotto';
import NumberBaseBall from './NumberBaseball';
const Games = () => {
  return;
  // 라우터 사용시 최상위로 감싸주워야함
  <BrowserRouter>
    <div>
      <Route path="/number-baseball" component={NumberBaseBall}></Route>
      <Route path="/rock-scissors-paper" component={RSP}></Route>
      <Route path="/lotto-generator" component={Lotto}></Route>
    </div>
  </BrowserRouter>;
};

export default Games;
