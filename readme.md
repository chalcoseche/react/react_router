#### section 8 (youtube)

> ## React Router 도입하기

- 웹개발 리액트용으로는 도움이 안될 수 있음.
- 웹게임 8개를 만들어 봤지만, 웹사이트는 어떻게 만들어야 할지 감이 안잡힘
- SPA 감을 잡기 위해 Router를 배움

- 리액트 router가 리액트에서 어떤역할, 웹사이트에서 어떤 역할을 하는지
- 연동하는방법

- 한페이지에서 8개의 게임을 선택할 수 있는 SPA제작하기
- npm i react-router 설치하기

- react-router는 웹/앱 둘다 사용가능 뼈대
- react-router-dom 이 필요함 ( 웹용 )

- 리액트 라우터가 여러개 페이지를 동시에 렌더링 해주기 떄문에
- 가상의 페이지 주소를 만들어서 컴포넌트를 연결 해준 것

> ## Link와 브라우저라우터(BrowserRouter)

- **리액트 라우터는 눈속임 ( 페이지가 여러개있는 척)**  
  (페이지가 존재 하지 않아, a태그가 작동을안함)
- Link라는 컴포넌트를 사용해야함

```
import {BrowserRouter,Link,Route}from 'react-router-dom';

```

(browser상에서는 a태그로 표시됨)

### 미래애서온 제로초

- hook을 임포트하면 에러가 뜸. _class 컴포넌트는 괜찮음_

  : _react hook을 두개의 대상이 다르면 문제가 됨_

해결법 : **class 컴포넌트 사용할 것**

이때; **@babel/plugin-proposal-class-properties 를 받아야 함.**

===

CANNOT GET / NUMBER-BASEBALL 에러 ;

- 주소를 입력하는 행위를 하면, 서버쪽에 전달하기 때문에 서버는 (컴포넌트(페이지)위치를 모름)
- 프론트엔드에서만 동작하는 행위 Route

  historyApiFallback:true;

> ## 해시라우터,params,withRouter

* BrowserRouter 와 HashRouter 차이
* 거의 비슷해 보이는데, 주소창에 # 이 들어가있음 (해시)
* 주소중간에 # 이들어가서 이상한 느낌 // 브라우저라우터는 깔끔한 주소
* 장점 : **새로고침해도 동작은 함** : 해시 뒤에 부분은 프론트엔드만 알고 있는 부분
* 서버에선 # 뒤에는 인식하지못함
* 단점 : 서버가 몰라서 SEO에서 불이익을 받음
* 검색엔진이 필요 없는 관리자 페이지 / SEO미적용 페이지에선 유효하다.
* ( 브라우저라우터도 SEO를 위해 몇가지 설정을 해야함)
* Route부분이 100개 1000 개많아지면 관리하기 어려움
* DynamicRouter 를 사용함. ( 동적 라우터 매칭)
* 라우터를 줄일 수 있는 방법
*  Path부분에 파람스가 들어감 `path="game/:name`
*  Link 태그의 path를 하나의 주소로 통일 시켜 (game)
*  Route는 하나로 처리할 수 있다.
*  리액트 라우터 의 가장 큰 부분 : history location match 가 undefined일 떄
*  라우트같은 컴포넌트에서 , ROUTE와 연결되어있을 때 뜸 
*  withRouter  : HOC 

> ## location,match,history
 *  **history** : 앞으로가기,뒤로가기 내역을 갖고 있음 눈속임(SPA)을 위한 메서드
 goback, go, goForward, push, replace 등 사용할 수 있음
 *  **match** : 매치안에 params안에 정보가 들어있다. (name 으로 분기 처리함)
 *  **loacation** : 이 주소에대한 정보 
```
this.props.match.params.name 과 같이 사용할 수 있음.
```
* history.pushState('','','','/hello');
* history API 로 주소를 바꿔줌 (브라우저의 히스토리, 리액트 히스토리와 다르다)
* 의존관계 정리해두기

> ## 쿼리스트링과 URLSearchParams
* 쿼리스트링 : ?query=106 (키=값) 형식으로 되어있음 (&로구분)
* 주소에  부가저인 데이터를 붙여줄 수 있음, (서버도 알아들음) 
* location / search 부분에 저장됨.
* new URLSearchParams(this.props.location.search.slice(1)) // ?을 slice로 제거
* 리액트가 기본적으로 제공하지 않기에 따로 파싱 해야함.
* Route바깥쪽부분은 레이아웃 (Link)
* 쿼리스트링은 보통 페이지 넘어갈때 사용함 (게시글,게시판,페이지)
* 부가적인 정보를 서버에게 보내서 ,새로고침시 해당하는 정보를 그대로 띄어줌 ( 구현을 직접 해야하긴 함)
* 이게 없었다면 새로 초기화됐을것 ( 첫번째 페이지로 )
* 쿼리스트링이 낫지  해시라우터는 잘 사용하지 않음 (앵커태그 시 ?)
* hoc 는 훅스로 대체가 가능하다. 

> ## render props,switch,exact
* 컴포넌트 대신에 render를 쓰는방법이 있음
* `render={(props)}`프롭스를 넘길 목적이라면 ,render를 사용할 것
*  만약에 여러 루트중에서 첫번째로 일치하는것만 하고싶을땐
* `<Switch>` 사용하면됨 의도하지않앗다면 Route 를 다 감싸주는게 국룰
*  path="/" 와 path:"/game/:name" 을 두개다 일치한다 생각함
*  `Exact path`는 저확하게 일치할시에만 작동을함 ;
*  상위주소 / 하위주소가 있을 때 Exact로 구별 해줘야함

> ## useLayoutEffect, useTransition, useDeferredValue
> react 18 버전에서 추가가됨 
> * 성능과 관련된부분은 보이진 않지만 사용자 편의 개선에 좋음
> * 첫번째 예제
> * setState를 useEffect로 바꾸는 경우도있음
> * 이럴때 SetName이 늦게 작동하기도함 (깜빡이는 느낌)
> *  실행순서를 알아야 수정 할 수 있음
> * Render는 virtual DOM 어떤 부분을 바꿔야할지 생각을함
> * useEffect는 화면을 그린후에 작동을 함 = 깜빡임이 보임
> * useEffect 를 useLayoutEffect로 바꾸면 깜빡임이 없어짐
> * useLayoutEffect를 언제 쓰느냐 : 평소 useEffect를 사용하다가 간발의 차이로 리렌더링 되는거같을때 고쳐주면 됨.
> * (실행순서를 앞당김)
> **반대로 실행순서를 뒤로 미루는방법**
> *  실시간 렌더링이 너무많아서 렉이 걸릴 수 있음
> * (최선의 방법은 : 많이 안만들면됨 ex)20개씩 자르기)
> * useTransition : startTransition 을 제공해서 
> * 바로 업데이트를 해야할것과 나중에 업데이트를 해야할것 구분하여 관리해줌
> * startTransition으로 감싸주면 그안에 있는 것들은 천천히(모아서 주기적으로 ) 렌더링 해줌
> * **단 : 업데이트행위가 즉시 일어나는건 startTransitio에 감싸면 안됨**
> useDeferredValue : result를 state대신 memo로 만들었을 때,
> `const deferredName = useDeferredValue(name);`
> * 덜중요한애들은 deferredName을 불러주면, 업데이트를 천천히 해준다.
> 
> 
> 정리 :  업데이트를 느리게 하려면 setTransition 안에 감싸기 , deferredName으로 명시해주기
>         useLayoutEffect : 업데이트를 빠르게하기 (렌더링보다)
>         사용자 편의 개선에 굉장히 좋음
